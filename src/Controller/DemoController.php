<?php
namespace App\Controller;

use App\Entity\Invitations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DemoController extends Controller
{
    public function __construct(){
        
    }
    
    public function html()
    {
        $result = $this->getDoctrine()
        ->getRepository(Invitations::class)
        ->findBy([]);
        
        $aOutput = [];
        foreach ($result as $invitation) {
            $aOutput[] = ['id'=>$invitation->getId()
                ,'subject'=>$invitation->getSubject()
                ,'sender'=>$invitation->getIdSender()
                ,'receiver'=>$invitation->getIdReceiver()
                ,'location'=>$invitation->getLocation()
                ,'description'=>$invitation->getDescription()
                ,'start_date'=>$invitation->getStartDate()
                ,'end_date'=>$invitation->getEndDate()
                ,'status'=>$invitation->getStatusAction()
                ,'create_date'=>$invitation->getCreateDate()
                ,'last_modify'=>$invitation->getLastModify()
            ];
        }
        
        $number = 90;
        return $this->render('demo/panel.html.twig', array(
            'number' => $number,
            'inviti' => $aOutput
        ));
    }
}