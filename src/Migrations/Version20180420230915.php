<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420230915 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invitations ADD firstname VARCHAR(30) NOT NULL, ADD lastname VARCHAR(30) NOT NULL, CHANGE subject subject VARCHAR(255) NOT NULL, CHANGE last_modify last_modify DATETIME NOT NULL, CHANGE create_date create_date DATETIME NOT NULL, CHANGE location location VARCHAR(255) NOT NULL, CHANGE description description LONGTEXT NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invitations DROP firstname, DROP lastname, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE subject subject VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE location location VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE create_date create_date DATETIME DEFAULT \'0000-00-00 00:00:00\' NOT NULL, CHANGE last_modify last_modify DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
