CREATE DATABASE `jam` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `invitations` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	  `id_sender` int(11) NOT NULL,
	  `id_receiver` int(11) NOT NULL,
	  `status_action` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
	  `start_date` datetime NOT NULL,
	  `end_date` datetime NOT NULL,
	  `last_modify` datetime NOT NULL,
	  `create_date` datetime NOT NULL,
	  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
	  `firstname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
	  `lastname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



